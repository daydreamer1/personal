package UML9172.grade;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Grade9172 {
	public static void main(String[] args) throws IOException {
		// 建立一个实例file导入网页
		File smallHtml = new File("src/small.html");
		File allHtml = new File("src/all.html");
		// 提取小班课网页中获得的分数数值(用函数实现)
		double myBefore, myBase, myTest, myProgram, myAdd;
		myBefore = myBase = myTest = myProgram = myAdd = 0.00;// 解析配置文件
		Properties properties = new Properties();
		// 使用InPutStream流读取properties文件，采用的是绝对路径，该方式的优点在于可以读取任意路径下的配置文件
		BufferedReader bufferedReader = new BufferedReader(new FileReader("src/total.properties"));
		properties.load(bufferedReader);
		// 提取配置文件中的总分数值
		double allBefore = Double.parseDouble(properties.getProperty("before"));
		double allBase = Double.parseDouble(properties.getProperty("base"));
		double allTest = Double.parseDouble(properties.getProperty("test"));
		double allProgram = Double.parseDouble(properties.getProperty("program"));
		double allAdd = Double.parseDouble(properties.getProperty("add"));
		myscore(smallHtml, allHtml, myBefore, myBase, myTest, myProgram, myAdd, allBefore, allBase, allTest, allProgram,
				allAdd);
	}

	// 获取我的分数的函数
	private static void myscore(File smallhtml, File allhtml, double myBefore, double myBase, double myTest,
			double myProgram, double myAdd, double allBefore, double allBase, double allTest, double allProgram,
			double allAdd) {
		// TODO Auto-generated method stub
		try {
			// 利用 jsoup将HTML解析成一个Document
			Document smalldata = Jsoup.parse(smallhtml, "UTF-8");
			Document alldata = Jsoup.parse(allhtml, "UTF-8");
			if (smalldata != null) {
				// 从网页源代码中可知显示经验值的盒子class为interaction-row
				// 用getElementsByAttributeValue将含有class="interaction-row"的目标找到
				Elements goal = smalldata.getElementsByAttributeValue("class", "interaction-row");
				double temp;

				// 开始遍历各个目标并筛选数据
				for (int i = 0; i < goal.size(); i++) {
					// 选择class=interaction-row为根节点
					Element rows_ChildElement = smalldata.getElementsByAttributeValue("class", "interaction-row")
							.get(i);

					if (rows_ChildElement.child(1).select("span").get(1).toString().contains("课堂完成")) {

						if (rows_ChildElement.child(1).child(2).toString().contains("已参与&nbsp")) {
							Scanner sc = new Scanner(
									goal.get(i).child(1).child(2).children().get(0).children().get(7).text());
							temp = sc.nextDouble();
							myBase = myBase + temp;
						}
					} else if (rows_ChildElement.child(1).child(0).select("span").get(1).toString().contains("课堂小测")) {
						if (rows_ChildElement.child(1).child(2).child(0).toString().contains("已参与&nbsp")) {
							Scanner sc = new Scanner(
									goal.get(i).child(1).child(2).children().get(0).children().get(7).text());
							temp = sc.nextDouble();
							myTest = myTest + temp;
						} else if (rows_ChildElement.child(1).child(0).select("span").get(1).toString()
								.contains("编程题")) {
							if (rows_ChildElement.child(1).child(2).child(0).toString().contains("已参与&nbsp")) {
								Scanner sc = new Scanner(
										goal.get(i).child(1).child(2).children().get(0).children().get(7).text());
								temp = sc.nextDouble();
								myProgram = myProgram + temp;
							}
						} else if (rows_ChildElement.child(1).child(0).select("span").get(1).toString()
								.contains("附加题")) {
							if (rows_ChildElement.child(1).child(2).child(0).toString().contains("已参与&nbsp")) {
								Scanner sc = new Scanner(
										goal.get(i).child(1).child(2).children().get(0).children().get(7).text());
								temp = sc.nextDouble();
								myAdd = myAdd + temp;
							}
						} else {
							if (goal.get(i).child(1).child(0).toString().contains("课前自测"))
								if (goal.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
									Scanner sc = new Scanner(goal.get(i).child(1).child(2).child(0).child(10).text());
									temp = sc.nextDouble();
									myBefore = myBefore + temp;
								}
						}
					}
				}
			}
			if (allhtml != null) {
				Elements goal2 = alldata.getElementsByClass("interaction-row");
				// 经验值
				double temp1;
				for (int i = 0; i < goal2.size(); i++) {
					if (goal2.get(i).child(1).child(0).toString().contains("课前自测")) {
						Scanner sc2 = new Scanner(
								goal2.get(i).child(1).child(2).children().get(0).children().get(10).text());
						temp1 = sc2.nextDouble();
						myBefore = myBefore + temp1;
					}
				}
			}
			double tbefore, tbase, ttest, tprogram, tadd, tsum;
			tbefore = myBefore / allBefore * 100 * 0.25;
			tbase = myBase / allBase * 100 * 0.3;
			ttest = myTest / allTest * 100 * 0.2;
			tprogram = myProgram / allProgram * 100 * 0.1;
			tadd = myAdd / allAdd * 100 * 0.05;
			tsum = (tbase + tbefore + ttest + tprogram + tadd) * 0.9 + 5;
			System.out.print(tsum);
		}
		// try必须和catch连用
		catch (IOException e) {
			System.out.print("ERROR");
		}
	}
}