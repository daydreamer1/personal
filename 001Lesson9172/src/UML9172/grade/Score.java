package UML9172.grade;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



public class Score {
 public static void main(String[] args) throws IOException {

  //导入配置文件
  Properties prop = new Properties();
  prop.load(new FileInputStream("src/total.properties"));
  Enumeration fileName = prop.propertyNames();

  //导入配置文件的键值
  double before = Integer.parseInt(prop.getProperty("before"));
  double base = Integer.parseInt(prop.getProperty("base"));
  double test = Integer.parseInt(prop.getProperty("test"));
  double program = Integer.parseInt(prop.getProperty("program"));
  double add = Integer.parseInt(prop.getProperty("add"));

  //利用 jsoup 解析网页文件
  File smallHtml = new File("src/small.html");
  File allHtml = new File("src/all.html");

  //计算获得的总分
  Interception(smallHtml, allHtml, before, base, test, program, add);
 }

 private static void Interception(File small_File, File all_File, double before, double base,
   double test, double program, double add) {

  int myBefore = 0;
  int myBase = 0;
  int myTest = 0;
  int myProgram = 0;
  int myAdd = 0;

  try {
 
	//解析html文件
	Document smallSource = Jsoup.parse(small_File, "UTF-8");
	Document allSource = Jsoup.parse(all_File, "UTF-8");


   if (smallSource != null) {
	// 将所有含有目标信息目标区域找出来
    Elements es = smallSource.getElementsByClass("interaction-row");
    int temp ;
 
 // 开始遍历各个目标区域并筛选数据
	for (int i = 0; i < es.size(); i++) {

		Element child = smallSource.select("div[class=interaction-row]").get(i);

		if (child.select("span").get(1).toString().contains("课堂完成")) {
			if (child.toString().contains("已参与")) {
				Scanner sc = new Scanner(child.select("span").get(9).text());
				temp = sc.nextInt();
				myBase = myBase + temp;
			}
		} else if (child.select("span").get(1).toString().contains("课堂小测")) {
			if (child.toString().contains("已参与")) {
				Scanner sc = new Scanner(child.select("span").get(9).text());
				temp = sc.nextInt();
				myTest = myTest + temp;
				// 筛选互评分部分
				if (child.toString().contains("+")) {
					Scanner sc2 = new Scanner(child.select("span").get(11).text());
					temp = sc2.nextLine().toString().charAt(3) - 48;
					myTest = myTest + temp;
				}
			}
		} else if (child.select("span").get(1).toString().contains("编程题")) {
			if (child.select("span").get(8).text().contains("已参与")) {
				Scanner sc = new Scanner(child.select("span").get(9).text());
				temp = sc.nextInt();
				myProgram = myProgram + temp;
			}
		} else if (child.select("span").get(1).toString().contains("附加题")) {
			if (child.select("span").get(8).text().contains("已参与")) {
				Scanner sc = new Scanner(child.select("span").get(9).text());
				temp = sc.nextInt();
				myAdd = myAdd + temp;
			}
		} else {
			if (child.select("span").get(1).toString().contains("课前自测")) {
				if (child.toString().contains("color:#8FC31F;")) {
					Scanner sc = new Scanner(child.select("span").get(12).text());
					temp = sc.nextInt();
					myBefore = myBefore + temp;
				}
			}
		}
	}
}

   if (allSource != null) {
    Elements es2 = allSource.getElementsByAttributeValue("class", "interaction-row");
    int temp2;
    for (int i = 0; i < es2.size(); i++)
		if (es2.get(i).select("span").get(1).toString().contains("课前自测")) {
			if (es2.get(i).toString().contains("color:#8FC31F")) {
				Scanner sc2 = new Scanner(es2.get(i).select("span").get(12).text());
				temp2 = sc2.nextInt();
				myBefore = myBefore + temp2;
			}
		}

   }


            // 单独计算各部分成绩
			double myFinalBefore = myBefore / before * 100 * 0.25;
			double myFinalBase = myBase / base * 100 * 0.3 * 0.95;
			double myFinalTest = myTest / test * 100 * 0.2;

			// 编程题的经验值最高分的定为 95 分
			double myFinalProgram = 0;
			if (myProgram >= 95) {
				myFinalProgram = 95 * 0.1;
			} else {
				myFinalProgram = myProgram / program * 100 * 0.1;
			}

			// 附加题的经验值最高分的定为 90 分
			double myFinalAdd = 0;
			if (myAdd >= 90) {
				myFinalAdd = 90 * 0.1;
			} else {
				myFinalAdd = myAdd / add * 100 * 0.05;
			}

			// 统计总分
			double myFinalScore = (myFinalBefore + myFinalBase + myFinalTest + myFinalProgram + myFinalAdd)
					* 0.9 + 6;
			System.out.println(String.format("%.2f", myFinalScore));

		} catch (IOException e) {
			System.out.println("解析出错！");
			e.printStackTrace();
		}
	}
}