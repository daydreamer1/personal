var MTWeb = {
    /**
     * 当 Ajax 调用返回值不是预期的业务逻辑值时，处理通用错误的显示
     * @param int resultCode 返回码
     * @param int subResultCode 子返回码
     * @param function callback 当异常信息显示完毕之后要调用的回调函数
     */
    showError: function(resultCode, subResultCode, callback) {
        var msg = '';
        switch (resultCode) {
            case -1:
                msg = '服务调用异常。错误码:' + subResultCode;
                break;
            case -2:
                msg = '服务返回异常，无法解析响应内容。错误码：' + subResultCode;
                break;
            case -9:
                msg = '请求后台服务失败。错误码：' + subResultCode;
                break;
            case 1001:
                msg = '请求参数有误';
                break;
            default:
                msg = '未知错误';
                break;
        }
        window.alert(msg);
        if (typeof callback === 'function') {
            callback.call(window, resp);
        }
    },
    /**
     * 如果 Ajax 调用返回码不是预期的，那么尝试从响应中解析 result_code 和 sub_result_code
     * @param object resp Ajax response object
     * @param function callback 当异常信息显示完毕之后要调用的回调函数
     */
    showAjaxError: function(resp, callback) {
        var jsonObject = resp;
        if (typeof resp === 'string') {
            try {
                jsonObject = JSON.parse(resp);
            } catch (error) {
                window.alert('无法解析响应内容');
                if (typeof callback === 'function') {
                    callback.call(window, resp);
                }
                return;
            }
        }

        var resultCode = (typeof jsonObject.result_code === 'undefined') ? -999 : jsonObject.result_code;
        var subResultCode = (typeof jsonObject.sub_result_code === 'undefined') ? -999 : jsonObject.sub_result_code;
        MTWeb.showError(resultCode, subResultCode, callback);
    },
    /**
    * 验证密码格式
    * @param string 密码串
    * return bool
    */
    chackPassword: function(data) {
        var isRightLength = /^.{8,16}$/.test(data);// 长度为8-16字符
        var hasLetter = /[a-zA-Z]+/.test(data);// 字母
        var hasNum = /[0-9]+/.test(data);// 匹配数字
        var hasSpecial = /[\~\!\@\#\$\%\^\&\*\(\)\_\+\-\/\.\?\[\]\{\}\<\>\=]+/.test(data);// 特殊字符/
        var hasSpace = /\s+/.test(data);// 空格
        var hasOtherChar = /[^\s\~\!\@\#\$\%\^\&\*\(\)\_\+\-\/\.\?\[\]\{\}\<\>\=0-9a-zA-Z]+/.test(data);// 非法字符

        hasSpecial = hasSpecial && !hasOtherChar;

        var changeIconState = function(flag, className) {
            if (flag) {//密码字符串中存在数字 icon亮起
                $('.password-authentication-box ' + className + ' .icon-exclamation-sign').attr('class','icon-ok-sign');
            } else {
                $('.password-authentication-box ' + className + ' .icon-ok-sign').attr('class','icon-exclamation-sign');
            }
        }

        changeIconState(isRightLength, '.msg-length');
        changeIconState(hasLetter, '.msg-letter');
        changeIconState(hasNum, '.msg-int');
        changeIconState(hasSpecial, '.msg-special');
        changeIconState(!hasSpace, '.msg-space');

        // 有非法字符 符号变红
        if (hasOtherChar) {
            $('.msg-special .allow-list').addClass('allow-list-error');
        } else {
            $('.msg-special .allow-list').removeClass('allow-list-error');
        }

        if (hasNum && hasLetter && !hasSpace && isRightLength && hasSpecial && !hasOtherChar) {
            return true;
        } else if (!isRightLength) {
            return 'passCheckLengthFailed';
        } else if (!hasLetter) {
            return 'passCheckLetterFailed';
        } else if (!hasNum) {
            return 'passCheckNumFailed';
        } else if (!hasSpecial || hasOtherChar) {
            return 'passCheckSpecialFailed';
        } else if (hasSpace) {
            return 'passCheckSpaceFailed';
        }
    },


    /**
     * 获取文件扩展名
     * @param  {string} fileName 文件名，包含扩展名部分
     * @return {string}          文件扩展名，不包含 . 符号。如果无法找到扩展名，那么返回 'dat'
     */
    getExtName: function(fileName) {
        var extName = 'dat';
        if ( ! fileName) return extName;
        var pos = fileName.lastIndexOf('.');
        if (pos === -1 || pos === fileName.length - 1) return extName;
        return fileName.substring(pos + 1);
    },

    /**
     * 获取不带扩展名的文件名
     * @param  {string} fileName The file name, for example: abcd.docx
     * @return {string}          The file name without extension name part. for example: abcd.docx will return abcd
     */
    getFileNameWithoutExt: function(fileName) {
        if (! fileName) return null;
        var pos = fileName.lastIndexOf('.');
        if (pos === -1) return fileName;
        return fileName.substring(0, pos);
    },

    /**
     * 为下拉选择时间进行格式化. HH:mm 格式的。
     * @param m int 分钟。
     * @return HH:mm 格式的字符串
     */
    formatTimeForCombobox: function(minutes) {
        var h = Math.floor(minutes / 60);
        var m = minutes - h * 60;
        return ('0' + h).substring(('0' + h).length - 2) + ':' + ('0' + m).substring(('0' + m).length - 2)
    },

    parseSecond: function(second) {
        second = Math.round(+second);
        var m = Math.floor(second / 60);
        var s = second - m * 60;
        var time = (m >= 10 ? m : '0' + m) + ':' + (s >= 10 ? s : '0' + s);
        return time;
    },

    /**
     * 显示如何切换浏览器极速模式
     * @param DOMElement
     */
    showSetBrowserToWebkit: function(ele) {
        var status = ele.attr('data-status');
        if (status === 'N') {
            $('.show-gif-box').stop().show();
            ele.attr('data-status', 'Y').text('收起');
        } else {
            $('.show-gif-box').stop().hide();
            ele.attr('data-status', 'N').text('极速模式');
        }
    },

    /**
     * 滑杆验证
     * @param object
     */
    sliderVerification: function(opt) {
        // if (!name) name = '#account-afs';
        var type = opt.type;
        var name = opt.name || '#account-afs';
        var cfKey = opt.cfKey;
        var nc_token = opt.nc_token;
        var callback = opt.callback;
        
        var nc;

        switch (type) {
            case 'PC':
                var NC_Opt = {
                    renderTo: name,
                    appkey: cfKey,
                    scene: 'nc_login',
                    token: nc_token,

                    trans:{"key1":"code0"},
                //  trans:{key1:'code300'},
                    elementID: ["usernameID"],
                    is_Opt: 0,
                    language: "cn",
                    isEnabled: true,
                    timeout: 3000,
                    times:5,
                    
                    callback: callback,
                }
                nc = new noCaptcha(NC_Opt);
                nc.upLang('cn', {
                    _startTEXT: "请按住滑块，拖动到最右边",
                    _yesTEXT: "验证通过",
                    _error300: "哎呀，出错了，点击<a href=\"javascript:__nc.reset()\">刷新</a>再来一次",
                    _errorNetwork: "网络不给力，请<a href=\"javascript:__nc.reset()\">点击刷新</a>",
                });
                break;
            case 'MOBILE':
                nc = NoCaptcha.init({
                    renderTo: name,
                    appkey: cfKey,
                    scene: 'nc_login_h5',
                    token: nc_token,
                    customWidth: 300,

                    is_Opt: 0,
                    language: "cn",
                    timeout: 10000,
                    retryTimes: 5,
                    errorTimes: 5,
                    inline: false,
                    apimap: {
                        // 'analyze': '//a.com/nocaptcha/analyze.jsonp',
                        // 'uab_Url': '//aeu.alicdn.com/js/uac/909.js',
                    },
                    bannerHidden: false,
                    initHidden: false,

                    callback: callback,
                    error: function (s) {
                        
                    }
                });

                NoCaptcha.setEnabled(true);
                nc.reset();//请务必确保这里调用一次reset()方法
                NoCaptcha.upLang('cn', {
                    'LOADING': "加载中...",//加载
                    'SLIDER_LABEL': "请向右滑动验证",//等待滑动
                    'CHECK_Y': "验证通过",//通过
                    'ERROR_TITLE': "非常抱歉，这出错了...",//拦截
                    'CHECK_N': "验证未通过", //准备唤醒二次验证
                    'OVERLAY_INFORM': "经检测你当前操作环境存在风险，请输入验证码",//二次验证
                    'TIPS_TITLE': "验证码错误，请重新输入"//验证码输错时的提示
                });
                break;
        }

        return nc;
    },
};

$(function(){
    // Register global ajax error handler
    $.ajaxSetup({
        error: function(jqXHR, textStatus, errorThrown) {
            hideLoading();
            console.log(textStatus);
            // 火狐浏览器多次连续刷新 ajax 会初始化失败走到 error 中，弹出提示框
            // alert('网络异常,请检查您的网络并重试(' + textStatus + ')');
        }
    });

    //返回顶部
    var windowWidth = $(window).width();//获取屏幕可视区域的宽度
    var windowHeight = $(window).height();//获取屏幕可视区域的高度
    //创建返回顶部按钮元素 距底部40px 距右20px
    var backTopButton = $('<div class="back-top-button">').attr('title','返回顶部').css('top',windowHeight - 100).css('left',windowWidth - 68);
    var backTopIcon = $('<img>').attr('src',"./common/images/icon-top.png");
        backTopIcon.appendTo(backTopButton);
        backTopButton.appendTo($('body'));
    var scrollTop = '';
    //设置返回顶部按钮显示的条件
    $(window).scroll(function() {
        scrollTop = $(window).scrollTop();
        if (scrollTop > 100) { //如果页面的滚动距离超过100像素 返回顶部按钮显示
            $('.back-top-button').fadeIn();
        } else { //否则影藏返回顶部按钮
            $('.back-top-button').fadeOut();
        }
    });
    $('.back-top-button').click( function () {
        $('body,html').animate({scrollTop:0},500);
        return false;
    });

    // 设置页面的最小高度
    $('#main').css('min-height',windowHeight - 152);
    $('#ccp-main').css('min-height', windowHeight - 193);
    $('#cc-main').css('min-height', windowHeight - 365);
    //设置页面无活动高度
    $('#search-data-empty').css('height',parseInt($('#cc-main').css('min-height')) - 247 + 'px');

    // 设置搜索框 搜索按钮获取焦点时颜色改变
    $('.search-box .search-button').mouseover(function (){
        $('.search-box .search-button').css('background','#00A3C3');
        $('.search-box input[name=search]').css('border','solid 1px #00A3C3');
    });
    $('.search-box .search-button').mouseout(function (){
        $('.search-box .search-button').css('background','#00BBDD');
        $('.search-box input[name=search]').css('border','solid 1px #00BBDD');
    });

    /**
     * input模拟可输入select
     */

    //input 获取焦点 下拉框显示
    $('.select-input .text-input').focus(function() {
        var self = $(this).parents('.select-input');
        var liSize = $(self).find('.options-box li').size();
        if (liSize <= 0) {
            return;
        }
        $(self).find('.options-box').stop().show();
    });

    // input 失去焦点 下拉框隐藏
    $('.select-input .text-input').blur(function() {
        var self = $(this).parents('.select-input');
        var selectStatus = $(self).attr('data-status');
        if (selectStatus === 'Y') {
            return;
        }
        $(self).find('.options-box').stop().hide();
    });

    $(document).on('mouseover', '.select-input .options-box', function() {
        $(this).parents('.select-input').attr('data-status', 'Y');
    });
    $(document).on('mouseout', '.select-input .options-box', function() {
        $(this).parents('.select-input').attr('data-status', 'N');
    });

    // 点击下拉框选项 将选项中的值赋给input
    $(document).on('click', '.select-input .options-box li', function() {
        var self = $(this).parents('.select-input');
        // 获取选项中的值
        // var optionText = $(this).text();
        var optionText = $(this).find('.text-input-value').text();
        // var identity = $(this).find('.identification').text();
        var optionValue = $(this).attr('data-value');

        $(self).find('.text-input').val(optionText);
        $(self).find('.input-value').val(optionValue);

        $(self).find('.options-box').stop().hide();
        $('.select-input .text-input').blur();
    });

    // 键盘事件
    var k = 0;
    $('.select-input .text-input').keyup(function(e) {
        var self = $(this).parents('.select-input');
        // 获取下拉框所有选项的个数
        var liSize = $(self).find('.options-box li').size();

        switch (e.keyCode) {
            case 38:
                if (k >= liSize) {
                    k = 0;
                }
                k++;
                $(self).find('.options-box li').removeClass('li-hover');
                $(self).find('.options-box li').eq(liSize - k).addClass('li-hover');
                break;
            case 40:
                if (k <= 0) {
                    k = liSize;
                }
                k--;
                $(self).find('.options-box li').removeClass('li-hover');
                $(self).find('.options-box li').eq(liSize - k - 1).addClass('li-hover');
                break;
            case 13:
                $(self).find('.li-hover').click();
                $(self).find('.text-input').blur();
                break;
        }
    });
});


/*
 * 提示弹窗
 * @param : String 弹窗标题
 * @param : String   提示的信息
 * @param : function 点击确定之后执行的操作
 * @param : function 点击取消之后执行的操作
 * @param : String 是否回收经验值()
 * @param : [alert] 可选 传入该参数 提示框没有取消按钮
 * @param : function 可选 传入该参数 提示框的右上关闭
 * @param : Strin 可选 确定按钮的文字 默认是“确定”
 * @param : Strin 可选 取消按钮的文字 默认是“取消”
 * @param : Boolean 可选 交换确认按钮的位置
*/
function confirms(msgTitle, msg, okHandler, cancelHandler, delExperience, type, closeConfirms, sureTip, cancelTip, isTogglePositon) {
    $('.tips-button-box-flex').removeClass('tips-button-box-flex');
    if (isTogglePositon) {
        $('.pop-tips-box .tips-button-box').addClass('tips-button-box-flex');
    }
    $('.tips-ok').text(sureTip || '确定');
    $('.tips-cancel').text(cancelTip || '取消');
    //显示弹窗
    $('.pop-tips-box').show();
    if (typeof (type) === 'undefined')
        type = '';

     if (type === 'alert') {//如果第五个参数全等语alert，提示框没有取消按钮
       $('.tips-cancel').hide();
       $('.tips-ok').css('margin-right', '0px');
     } else { //否则，提示框有取消按钮
       $('.tips-cancel').show();
       $('.tips-ok').css('margin-right', '15px');
     }
    // 设置提示的标题和内容
    $('.pop-tips-box .title-content').text(msgTitle);
    $('.pop-tips-box .msg-content').html(msg);

    var _width = $('.pop-tips-box').width();//获取提示框遮罩的宽度
    var _height = $('.pop-tips-box').height();//获取提示框遮罩的宽度
    var tipsWidth = $('.samll-tips-box').width();//获取提示框的宽度
    var tipsHeight = $('.samll-tips-box').height();//获取提示框的高度

    if ( typeof(delExperience) === 'undefined')
        delExperience = 'N';

    // 显示回收经验
    if (delExperience === 'Y') {
        $('#remove-experience').show();
    } else {
        $('#remove-experience').hide();
    }

    $('#remove-experience i').unbind('click');
    $('#remove-experience i').click(function() {
        // alert(1)
        var isDelExperience = $('#del-experience-input').val();
        if (isDelExperience === 'N') {
            $(this).attr('class', 'icon-check');
            $('#del-experience-input').val('Y');
        } else {
            $(this).attr('class', 'icon-check-empty');
            $('#del-experience-input').val('N');
        }
    });

    // 设置提示框的位置
    $('.samll-tips-box').css('top',(_height - tipsHeight) / 2).css('left',(_width - tipsWidth) / 2);
    // 设置的提示框高度超过600px时，加上滚动条
    if (tipsHeight > 600) {
       $('.msg-content').css('height','464px').css('overflow','auto');
       $('.samll-tips-box').css('top',(_height - 600) / 2)
    }

    $('.tips-ok').unbind('click');
    $('.tips-cancel').unbind('click');
    // 点击确定 执行okHandler里的方法
    $('.tips-ok').click( function (){
        if($(this).hasClass('tips-ok-disable')){
            return;
        }
        if (typeof okHandler === 'function') {
            okHandler.apply(window);
        }
        $('.pop-tips-box').hide();
        $('#remove-experience i').attr('class', 'icon-check-empty');
        $('#del-experience-input').val('N');
    });

    // 点击取消 执行cancelHandler里的方法
    $('.tips-cancel').click( function (){
        if (typeof cancelHandler === 'function') {
            cancelHandler.apply(window);
        }
        $('.pop-tips-box').hide();
        $('#remove-experience i').attr('class', 'icon-check');
        $('#del-experience-input').val('Y');
    });

    $('.pop-tips-box .close-button').mouseover(function (){
        $('.pop-tips-box .close-button').attr('src','./common/images/icon-close-hover.png');
    });
    $('.pop-tips-box .close-button').mouseout(function (){
        $('.pop-tips-box .close-button').attr('src','./common/images/icon-close-normal.png');
    });
    // 点击右上角的关闭按钮 影藏提示框
    $('.pop-tips-box .close-button').click( function (){
        if (typeof closeConfirms === 'function') {
            closeConfirms();
        }
        $('.pop-tips-box').hide();
        $('#remove-experience i').attr('class', 'icon-check-empty');
        $('#del-experience-input').val('N');
    });
}


/**
 * 提示框2（从顶部下滑，1600毫秒后上升）
 * @param String 提示语
 * @param Int 下滑的时间
 * @param Int 停留的时间
 * @param Int 上滑的时间
 * @param Int 距离顶部的高度
 */
function successTips (msg, downTime, stopTime, upTime, top) {

    // 设置提示语
    $('.success-tips .success-tips-msg').text(msg);

    var _width = $(window).width();// 获取屏幕可见宽度
    var tipsWidth = $('.success-tips').width();//获取提示框的宽度

    //设置提示框显示的位置
    $('.success-tips').css('left',(_width - tipsWidth) / 2);

    // 设置下滑的时间
    var topValue = top || '0px'
    $('.success-tips').animate({
        "top":topValue
    },downTime);

    // 设置停留的时间
    setTimeout( function () {
        $('.success-tips').animate({
            "top":'-43px'
        },upTime);
    },stopTime);
}

/**
 * 浮层提示
 * @param String 提示信息
 * @param DOMElement 显示的位置
 */
function layerMsg(msg, obj, focus) {
    layer.tips(msg, obj, {
        time: 3000,
        tips: [1, '#F26C4F'] //还可配置颜色
    });
    if(focus !== false) obj.focus();
}
function layerPop(msg, obj) {
    layer.tips(msg, obj, {
        time: 9999999,
        skin: 'layer-pop-alert',
        tips: [1, '#fff'] //还可配置颜色
    });
}

function showLoading() {
    var w = $(document).width();
    var h = $(document).height();
    $('#loading_bg').width(w + "px")
                    .height(h + "px")
                    .show();
    var w = $(window).width();
    var h = $(window).height();
    $('#loading').css('top', $(window).scrollTop() + (h - 64) / 2 + "px")
                .css('left', $(window).scrollLeft() + (w - 64) / 2 + "px")
                .show();
    $('#close-loading').show();
}

function hideLoading() {
    $('#loading_bg').hide();
    $('#loading').hide();
    $('#close-loading').hide();
}
// 停止加载
function closeLoading () {
    if (typeof continueLoading != 'undefined') {
        var now = continueLoading || false;
        if (now) {
            continueLoading = 'N';
        }
    }
    hideLoading();
}
/**
 * 顶部提示
 * @param string 提示的信息
 * @param string 按钮的文字
 */
function topTips(text, buttonText) {
    $('#top-tips-text').text(text);
    $('#top-tips-button').text(buttonText);
    $('#top-tips').slideDown();
    setTimeout(function() {
        $('#top-tips').slideUp(500);
    }, 10000);
    $('#top-tips-button').click(function() {
        $('#top-tips').slideUp(500);
    });
}

/**
 * 根据实际尺寸获取等比例缩放尺寸
 * @param iwidth
 * @param iheight
 * @param width
 * @param height
 * @returns {{width: *, height: *, top: number, left: number}}
 * @constructor
 */
function DrawImage(iwidth, iheight, width, height) {
    var param = {
        width: width,
        height: height,
        top: 0,
        left: 0
    }
    if (width > 0 && height > 0) {
        if (width / height >= iwidth / iheight) {
            if (width > iwidth) {
                param.width = iwidth;
                param.height = (height * iwidth) / width;
            } else {
                param.width = width;
                param.height = height;
            }
        } else {
            if (height > iheight) {
                param.height = iheight;
                param.width = (width * iheight) / height;
            } else {
                param.width = width;
                param.height = height;
            }
        }
        param.left = Math.round((iwidth - param.width) / 2);
        param.top = Math.round((iheight - param.height) / 2);
    }
    return param;
}

/**
 * jeasyui 日期框的日期格式　YYYY-MM-DD
 */
function myformatter(date){
    if (! date) return;
    var y = date.getFullYear();
    var m = date.getMonth()+1;
    var d = date.getDate();
    return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
}
function myparser(s){
    if (!s) return new Date();
    var ss = (s.split('-'));
    var y = parseInt(ss[0],10);
    var m = parseInt(ss[1],10);
    var d = parseInt(ss[2],10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
        return new Date(y,m-1,d);
    } else {
        return new Date();
    }
}

// 判断 easyui datebox 日期是否可选 (自动结束活动)
function easyuiValidator (date) {
    var now = new Date();
    var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    if (date < d1) {
        return false;
    } else {
        if (date.getMonth() == now.getMonth() && date.getDate() == now.getDate() && now.getHours() >= 23 && now.getMinutes() >= 59) {
            return false
        } else {
            return true;
        }
    }
}
// 替代 easyuiReloadHours 方法。 date [String] "2018-01-01"
function enableComboboxHours(date, selectedHour) {
    var n = 0;
    var d = date.split('-');
    var selectedDate = new Date(Number(d[0]), Number(d[1])-1, Number(d[2]));
    selectedHour = Number(selectedHour || 0);
    var current = new Date();
    // today
    var sameYear = current.getFullYear() === selectedDate.getFullYear();
    var sameMonth = current.getMonth() === selectedDate.getMonth();
    var sameDate = current.getDate() === selectedDate.getDate();
    if (sameYear && sameMonth && sameDate) {
        var a_h = Math.abs(current.getHours());
        var a_m = Math.abs(current.getMinutes());
        // 当前分钟为59， 则小时数的最小值为下一小时
        if (a_m >= 59) {
            n = a_h + 1;
        } else {
            n = a_h;
        }
    }
    var hoursArr = [];
    for (var i = n; i <= 23; i++) {
        var hour = {
            selected: false,
            value: i,
            text: i < 10 ? ('0' + i.toString()) : i.toString()
        };
        hoursArr.push(hour);
    }
    return hoursArr;
}
// 替代 easyuiReloadMinutes 方法 date, hour
function enableComboboxMinutes(date, hour, selectedMinute) {
    var n = 0;
    var current = new Date();
    var d = date.split('-');
    var selectedDate = new Date(Number(d[0]), Number(d[1]) - 1, Number(d[2]));
    hour = Number(hour);
    selectedMinute = Number(selectedMinute || 0);
    // today
    var sameYear = current.getFullYear() === selectedDate.getFullYear();
    var sameMonth = current.getMonth() === selectedDate.getMonth();
    var sameDate = current.getDate() === selectedDate.getDate();
    var sameHours = current.getHours() == hour;

    if (sameYear && sameMonth && sameDate && sameHours) {
        var currentMinute = Math.abs(current.getMinutes());
        n = currentMinute + 1;
    }
    var minutesArr = [];
    for (var i = n; i <= 59; i++) {
        var minute = {
            selected: false,
            value: i < 10 ? ('0' + i.toString()) : i.toString(),
            text: i < 10 ? ('0' + i.toString()) : i.toString()
        };
        minutesArr.push(minute);
    }
    return minutesArr;
}
/**
 * easyui databox 选择日期后  重新加载小时
 * param dateBox 日期选择框
 * return combobox 小时数据
 */
function easyuiReloadHours (dateBox) {
    var n = 0;
    var selectedDate = new Date(dateBox.datebox('getValue'));
    var current = new Date();
    // today
    var sameYear = current.getFullYear() === selectedDate.getFullYear();
    var sameMonth = current.getMonth() === selectedDate.getMonth();
    var sameDate = current.getDate() === selectedDate.getDate();
    if (sameYear && sameMonth && sameDate) {
        var a_h = Math.abs(current.getHours());
        var a_m = Math.abs(current.getMinutes());
        // 当前分钟为59， 则小时数的最小值为下一小时
        if (a_m >= 59) {
            n = a_h + 1;
        } else {
            n = a_h;
        }
    }
    var hoursArr = [];
    for (var i = n; i <= 23; i++) {
        var hour = {
            selected: i === n,
            value: i < 10 ? ('0' + i.toString()) : i.toString(),
            text: i < 10 ? ('0' + i.toString()) : i.toString()
        };
        hoursArr.push(hour);
    }
    return hoursArr;
}

/**
 * 分钟选择器数据
 * param dateBox 日期选择框 jquery 对象
 * param hours 选中的小时
 * return 分钟数据
 */
function easyuiReloadMinutes (dateBox, hours) {
    var n = 0;
    var current = new Date();
    var selectedDate = new Date(dateBox.datebox('getValue'));
    // today
    var sameYear = current.getFullYear() === selectedDate.getFullYear();
    var sameMonth = current.getMonth() === selectedDate.getMonth();
    var sameDate = current.getDate() === selectedDate.getDate();
    var sameHours = current.getHours() == hours;

    if (sameYear && sameMonth && sameDate && sameHours) {
        var currentMinute = Math.abs(current.getMinutes());
        n = currentMinute + 1;
    }
    var minutesArr = [];
    for (var i = n; i <= 59; i++) {
        var minute = {
            selected: i === n,
            value: i < 10 ? ('0' + i.toString()) : i.toString(),
            text: i < 10 ? ('0' + i.toString()) : i.toString()
        };
        minutesArr.push(minute);
    }
    return minutesArr;
}

// 选择日期后重载时间（时间范围有变化，限定最小时间）
function easyuiReloadTime (dateEle) {
    var date = dateEle.combobox('getValue');
    var selectedDate = new Date(date);

    var n = 0;
    var a = new Date();
    // today
    if (a.getMonth() === selectedDate.getMonth() && a.getDate() === selectedDate.getDate()) {
        var a_h = Math.abs(a.getHours());
        var a_m = Math.abs(a.getMinutes());

        n = Math.ceil(a_h * 2 + a_m / 30);
    }

    var timeArr = [];
    for (var i = n; i < 48; i++) {
        var t = Math.floor(i / 2);
        var h = t > 9 ? t : '0' + t;
        var m = i % 2 == 0 ? '00' : '30';
        var selected = (i == n) ? true : false;

        var row = {
            selected: selected,
            value: h + ':' + m,
            text: h + ':' + m
        };

        timeArr.push(row);
    }
    return timeArr;
}

/**
 * 使用 jeasyui 的日期框时 判断选中的日期是否等于当前日期，如果等于当前日期， 时间框的只显示大于当前时间的选项
 * （此方法只在资源的发布设置中使用）
 * @param [type] int
 * @param [type] DOMElement
 */
function getSelectedDate(number, self) {
    var date = new Date();
    var y = date.getFullYear();
    var m = (date.getMonth() + 1 <= 9) ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
    var d = (date.getDate() <= 9) ? '0' + date.getDate() : date.getDate();

    var dateStr = y + '-' + m + '-' + d;

    var setDate = $('#release-date').datebox('getValue');

    var hours = date.getHours();
    var minutes = date.getMinutes();
        minutes = minutes < 10 ? '0' + minutes : minutes;
        hours = hours < 10 ? '0' + hours : hours;
    var time = hours + "" + minutes;

    var dateBox = $(self).parents('.panel').find('.date-box');
    var h = 0;

    if (dateStr === setDate) {
        if (minutes > 30) {
            h = date.getHours() * 2 + 2;
        } else{
            h = date.getHours() * 2 + 1;
        }
    }

    var timearr = [];
    for (var i = h; i < 48; i++) {
        timearr.push({
            id: i,
            text: '',
            value: i * 30
        });
    }
    timearr[0]['selected'] = true;
    $('#release-time').combobox('loadData', timearr);

    if (number === 1) {
        $('#release-file-type').val('TIMED');
        $('#release_time').css('display', 'table-cell');
    }

    $('#release-file-time').val(setDate + ' ' + timearr[0]['text'] + ':00');
};

function mi_nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
/**
 * html  转义
 * 注意: 两个空格替换成一个空格加一个 &nbsp
 * 一段英文，单词之间的一个空格被替换成 &nbsp 后，不会自动换行了,so 两个空格替换成一个空格加一个 &nbsp，一个空格的情况不用替换
 * @param  string str   要转义的字符串
 * @return string       转义后的字符串
 */
function html_encode(str) {
  str = (str === null) ? '' : str;
  var s = "";
  if (str.length == 0) return "";
  s = str.replace(/&/g, "&amp;");
  s = s.replace(/</g, "&lt;");
  s = s.replace(/>/g, "&gt;");
  s = s.replace(/\s{2}/g, " &nbsp;");
  s = s.replace(/\'/g, "&#39;");
  s = s.replace(/\"/g, "&quot;");
  s = s.replace(/\n/g, "<br>");
  return s;
}
function html_decode(str) {
  var s = "";
  if (str.length == 0) return "";
  s = str.replace(/&gt;/g, "&");
  s = s.replace(/&lt;/g, "<");
  s = s.replace(/&gt;/g, ">");
  s = s.replace(/&nbsp;/g, " ");
  s = s.replace(/&#39;/g, "\'");
  s = s.replace(/&quot;/g, "\"");
  s = s.replace(/<br>/g, "\n");
  return s;
}


function get_browser(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name:'MSIE',version:(tem[1]||'')};
        }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
      name: M[0],
      version: M[1]
    };
 }

function vTime(time) {

    var theSecond = parseInt(time);
    var theMinute = 0;
    var theHour = 0;
    if(theSecond > 60){
        theMinute=parseInt(theSecond / 60);
        theSecond=parseInt(theSecond % 60);

        if(theMinute > 60){
            theHour = parseInt(theMinute / 60);
            theMinute = parseInt(theMinute % 60);
        }
    }
    var result = "" + parseInt(theSecond) + " 秒";
    if(theMinute > 0) {
        result = "" + parseInt(theMinute) + " 分 " + result;
    }
    if(theHour > 0) {
        result = "" + parseInt(theHour) + " 小时 " + result;
    }
    return result;
}

/**
 * 获取活动类型名称
 * @param  string $type
 * @return string
 */
function getActTypeName (type)  {
    switch (type)  {
        case 'VOTE':
            return "投票问卷";
        case 'STORM':
            return "头脑风暴";
        case 'QUIZ':
            return "测试";
        case 'QA':
            return "轻直播/讨论";
        case 'HOMEWORK':
            return "作业/小组任务";
        default:
            return "未知";
    }
}

function initPagenation() {
    $('#search-form').submit(function(){
        if ($("#type").val() === 'cr') {
            $("#page-index").val("1");
        }
        return true;
    });
    var pageIndex = parseInt($("#page-index").val());
    var pageCount = parseInt($("#page-count").val());
    if (!pageIndex) pageIndex = 1;
    if (!pageCount) pageCount = 1;
    if (pageIndex == 1) {
        $("#frist-page").attr('data-status', 'N');
        $("#pre-page").attr('data-status', 'N');
    } else if (pageIndex > 1) {
        $("#frist-page").attr('data-status', 'Y');
        $("#pre-page").attr('data-status', 'Y');
        $("#frist-page").click(function(e) {
            $("#page-index").val("1");
            $("#type").val('lf');
            $("#search-form").submit();
        });
        $("#pre-page").click(function(e) {
            $("#page-index").val(pageIndex - 1);
            $("#type").val('lf');
            $("#search-form").submit();
        });
    }

    if (pageIndex == pageCount) {
        $("#next-page").attr('data-status', 'N');
        $("#last-page").attr('data-status', 'N');
    } else if (pageIndex < pageCount) {
        $("#next-page").attr('data-status', 'Y');
        $("#last-page").attr('data-status', 'Y');
        $("#next-page").click(function(e) {
            $("#page-index").val(pageIndex + 1);
            $("#type").val('lf');
            $("#search-form").submit();
        });
        $("#last-page").click(function(e) {
            $("#page-index").val(pageCount);
            $("#type").val('lf');
            $("#search-form").submit();
        });
    }
}

// 检测下拉列表为空时隐藏
function panelVisible (comboboxObj) {
    var panel = comboboxObj.combobox('panel');
    var searchResultCount = panel.children('div:visible').length;
    if (searchResultCount == 0) {
        panel.panel('close');
    }
};
// 为 combobox 的 panel 绑定面板打开事件
function bindPanelOpenEvent (comboboxObj) {
    console.log(comboboxObj);
    
    var panel = comboboxObj.combobox('panel');
    panel.panel({
        onOpen: function(e){
            console.log(e);
            panelVisible(comboboxObj);
        }
    });

    // 修复点击输入清空
    setTimeout(function() {

        comboboxObj.siblings('.textbox').find('.textbox-text').on('keydown', function(e) {
            if (e.keyCode !== 13) {
                var that = this;
                setTimeout(function() {
                    _moso.easyuiTextboxVlaue = $(that).val();
                },0);
            } else {
                if (!_moso.easyuiTextboxVlaue) return;
                $(this).val(_moso.easyuiTextboxVlaue || '');
            }
    
        });
    }, 0);
};
// 通用的错误码
// ccId 在 frame_view 中赋值
var commonError = {
    errorMsgMap: {
        '1001': '操作无效，缺少必要参数（1001）',
        '1007': '操作无效，活动已不在进行中 （1007）',
        '2002': '操作无效，你已离开此班课 （2002）',
        '2003': '操作无效，班课已被删除 （2003）',
        '2004': '该成员已不在此班课中 (2004)',
        '2006': '操作无效，班课已结束 （2006）',
        '2007': '操作无效，此活动已被删除 （2007）',
        '2011': '作业活动已变为进行中，请重新进入活动 （2011）',
        '2013': '操作无效，此资源已被删除 （2013）',
        '1002': '抱歉，你没有权限 （1002）',
        '1802': '这个作业已被指定重新提交 （1802）',
        '4003': '权限发生变化，请返回班课列表重新操作 （4003）',
        '4005': '你没有创建班课的权限，如需帮助请联系客服。教师交流服务QQ群：836483291 (4005)',
        '4006': '由于技术原因，我们需要暂时关闭班课成员之间的私聊功能，很抱歉给你带来不便，敬请谅解 (4006)',
        '4007': '操作无效，你的账号已被封禁 (4007)',
        '-9' : '网络异常,请检查您的网络并重试',
        'undefined': '发生未知错误'
    },
    url: '',
    ccId: '',
    actId: '',
    handleFunc: function(errCode,sub_result_code, msg){
        if (typeof hideLoading === 'function') hideLoading();
        switch (errCode) {
            case 401:
                window.location.href = 'index.php?c=passport&m=index';
                break;
            case 1007:
            case 2007:
                if (this.ccId)
                    this.url = 'index.php?c=interaction&m=index&clazz_course_id=' + this.ccId; // 活动列表
                else
                    this.url = 'index.php?c=clazzcourse&m=index'; // 班课列表
                break;
            case 2002:
            case 2003:
            case 4003:
            case 2006:
                this.url = 'index.php?c=clazzcourse&m=index'; // 班课列表
                break;
            case 1802:
                if (this.actId)
                    this.url = 'index.php?c=clazzcourse&m=index&id=' + this.actId; // 作业首页
                else
                    this.url = 'index.php?c=interaction&m=index&clazz_course_id=' + this.ccId; // 活动列表

                break;
            case 1002:
                this.url = '';
                break;
            case -9:
                this.url = '';
                break;
            case 2013:
                this.url = location.href;
                break;
            case 4008:
                this.url = 'index.php?c=interaction&m=index&clazz_course_id=' + this.ccId; // 活动列表
                this.errorMsgMap[errCode] = msg && msg.result_msg || '';
                break;
            default:
                this.url = '';
                break;
        }
        var self = this;
        errCode = String(errCode);
        var errorMsg = '';
        if (errCode == '-9'){
            errorMsg = self.errorMsgMap[errCode] + ' （' + errCode + '）';
        }else{
            errorMsg = self.errorMsgMap[errCode];
        }
        confirms('提示', errorMsg || self.errorMsgMap['undefined'], function(){
            if (self.url)
                window.location.href = self.url;
        }, '', '', 'alert', function(){
            if (self.url)
                window.location.href = self.url;                
        });
        return;
    }
}

// easyui combobox 下拉框内容强制不换行，超出省略
function comboboxListItem (text) {
    text = html_encode(text);
    return '<div class="overflow-ellipsis" style="cursor: pointer" title="' + text + '">' + text + '</div>';
}
// 检测 emoji 表情的方法
function isEmojiCharacter(substring) {
    for ( var i = 0; i < substring.length; i++) {
        var hs = substring.charCodeAt(i);
        if (0xd800 <= hs && hs <= 0xdbff) {
            if (substring.length > 1) {
                var ls = substring.charCodeAt(i + 1);
                var uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                if (0x1d000 <= uc && uc <= 0x1f77f) {
                    return true;
                }
            }
        } else if (substring.length > 1) {
            var ls = substring.charCodeAt(i + 1);
            if (ls == 0x20e3) {
                return true;
            }
        } else {
            if (0x2100 <= hs && hs <= 0x27ff) {
                return true;
            } else if (0x2B05 <= hs && hs <= 0x2b07) {
                return true;
            } else if (0x2934 <= hs && hs <= 0x2935) {
                return true;
            } else if (0x3297 <= hs && hs <= 0x3299) {
                return true;
            } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030
                    || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b
                    || hs == 0x2b50) {
                return true;
            }
        }
    }
}
// 格式化时间
Date.prototype.Format = function (fmt) {
    var o = {
        "y+": this.getFullYear(),
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        "h+": this.getHours(),                   //小时
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S+": this.getMilliseconds()             //毫秒
    };
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)){
            if(k == "y+"){
                fmt = fmt.replace(RegExp.$1, ("" + o[k]).substr(4 - RegExp.$1.length));
            }
            else if(k=="S+"){
                var lens = RegExp.$1.length;
                lens = lens==1?3:lens;
                fmt = fmt.replace(RegExp.$1, ("00" + o[k]).substr(("" + o[k]).length - 1,lens));
            }
            else{
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
    }
    return fmt;
}
// 获取本地图片路径
function  getObjectURL(file)
{
    var url = '';
    if(window.createObjectURL != undefined)
    {
        url = window.createObjectURL(file);
    }else if(window.URL != undefined)
    {
        url = window.URL.createObjectURL(file);
    }else if(window.webkitURL != undefined)
    {
        url = window.webkitURL.createObjectURL(file);
    }
    return url;
}


var _moso = {
    getUseType: function(type) {
        var useType;
        switch (type) {
            case 'BEFORE_CLASS':
                useType = '课前';
                break;
            case 'IN_CLASS':
                useType = '课中';
                break;
            case 'AFTER_CLASS':
                useType = '课后';
                break;
            case 'PRACTICE':
                useType = '实践';
                break;
            case 'MID_TERM':
                useType = '期中';
                break;
            case 'FINAL_TERM':
                useType = '期末';
                break;
            case 'OTHER':
                useType = '未指定';
                break;
            default:
                useType = '未指定';
        }
        return useType;
    },
    htmlDecode: function(text) {
        //html  转义
        return text
            .replace(/&amp;/g, "&")
            .replace(/&lt;/g, "<")
            .replace(/&gt;/g, ">")
            .replace(/&quot;/g, '"')
            .replace(/&#039;/g, "'");
    } 
}